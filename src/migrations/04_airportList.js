const Sequelize = require('sequelize');

module.exports = {
	up: async (queryInterface) => {
		await queryInterface.createTable('airport_list', {
			id: {
				type: Sequelize.INTEGER,
				autoIncrement: true,
				primaryKey: true,
			},
			location_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'location',
					key: 'id',
				},
				allowNull: false,
			},
			airport_name: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			airport_code: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			createdAt: {
				type: Sequelize.DATE,
			},
			updatedAt: {
				type: Sequelize.DATE,
			},
		});
	},
	down: async (queryInterface) => {
		await queryInterface.dropTable('airport_list');
	},
};
