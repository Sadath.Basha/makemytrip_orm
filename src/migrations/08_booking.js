const Sequelize = require('sequelize');

module.exports = {
	up: async (queryInterface) => {
		await queryInterface.createTable('booking', {
			id: {
				type: Sequelize.INTEGER,
				autoIncrement: true,
				primaryKey: true,
			},
			flight_price_details_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'flight_price_details',
					key: 'id',
				},
				allowNull: false,
			},
			user_id: {
				type: Sequelize.STRING,
				references: {
					model: 'users',
					key: 'id',
				},
				allowNull: false,
			},
			confirm_status: {
				type: Sequelize.INTEGER,
				allowNull: false,
			},
			payment_status: {
				type: Sequelize.INTEGER,
				allowNull: false,
			},
			createdAt: {
				type: Sequelize.DATE,
			},
			updatedAt: {
				type: Sequelize.DATE,
			},
		});
	},
	down: async (queryInterface) => {
		await queryInterface.dropTable('booking');
	},
};
