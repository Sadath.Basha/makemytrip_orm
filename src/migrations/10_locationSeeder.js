module.exports = {
	up: async (queryInterface) => {
		const date = new Date();
		await queryInterface.bulkInsert(
			'location',
			[
				{
					country: 'India',
					state: 'West Bengal',
					city: 'Kolkata',
					createdAt: date,
					updatedAt: date,
				},
				{
					country: 'India',
					state: 'Karnataka',
					city: 'Bangalore',
					createdAt: date,
					updatedAt: date,
				},
				{
					country: 'India',
					state: 'Maharashtra',
					city: 'Mumbai',
					createdAt: date,
					updatedAt: date,
				},
				{
					country: 'India',
					state: 'Haryana',
					city: 'Delhi',
					createdAt: date,
					updatedAt: date,
				},
				{
					country: 'India',
					state: 'Telangana',
					city: 'Hyderabad',
					createdAt: date,
					updatedAt: date,
				},
			],
			{}
		);
	},

	down: async (queryInterface) => {
		await queryInterface.bulkDelete('location', null, {});
	},
};
