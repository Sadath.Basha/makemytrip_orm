const Sequelize = require('sequelize');

module.exports = {
	up: async (queryInterface) => {
		await queryInterface.createTable('flights', {
			id: {
				type: Sequelize.INTEGER,
				autoIncrement: true,
				primaryKey: true,
			},
			airport_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'airport_list',
					key: 'id',
				},
				allowNull: false,
			},
			flight_code: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			from_location_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'location',
					key: 'id',
				},
				allowNull: false,
			},
			to_location_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'location',
					key: 'id',
				},
				allowNull: false,
			},
			createdAt: {
				type: Sequelize.DATE,
			},
			updatedAt: {
				type: Sequelize.DATE,
			},
		});
	},
	down: async (queryInterface) => {
		await queryInterface.dropTable('flights');
	},
};
