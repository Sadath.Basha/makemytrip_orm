module.exports = {
	up: async (queryInterface) => {
		const date = new Date();
		await queryInterface.bulkInsert(
			'flight_class',
			[
				{
					name: 'First Class',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'Business Class',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'Economy Class',
					createdAt: date,
					updatedAt: date,
				},
			],
			{}
		);
	},

	down: async (queryInterface) => {
		await queryInterface.bulkDelete('flight_class', null, {});
	},
};
