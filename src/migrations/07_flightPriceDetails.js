const Sequelize = require('sequelize');

module.exports = {
	up: async (queryInterface) => {
		await queryInterface.createTable('flight_price_details', {
			id: {
				type: Sequelize.INTEGER,
				autoIncrement: true,
				primaryKey: true,
			},
			flight_departure_details_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'flight_departure_details',
					key: 'id',
				},
				allowNull: false,
			},
			class_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'flight_class',
					key: 'id',
				},
				allowNull: false,
			},
			price: {
				type: Sequelize.TIME,
				allowNull: false,
			},
			createdAt: {
				type: Sequelize.DATE,
			},
			updatedAt: {
				type: Sequelize.DATE,
			},
		});
	},
	down: async (queryInterface) => {
		await queryInterface.dropTable('flight_price_details');
	},
};
