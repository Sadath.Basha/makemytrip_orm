const Sequelize = require('sequelize');

module.exports = {
	up: async (queryInterface) => {
		await queryInterface.createTable('flight_departure_details', {
			id: {
				type: Sequelize.INTEGER,
				autoIncrement: true,
				primaryKey: true,
			},
			flight_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'airport_list',
					key: 'id',
				},
				allowNull: false,
			},
			departure_date: {
				type: Sequelize.DATE,
				allowNull: false,
			},
			departure_time: {
				type: Sequelize.TIME,
				allowNull: false,
			},
			arrival_date: {
				type: Sequelize.DATE,
				allowNull: false,
			},
			arrival_time: {
				type: Sequelize.TIME,
				allowNull: false,
			},
			capacity: {
				type: Sequelize.INTEGER,
				allowNull: false,
			},
			luggage_capacity: {
				type: Sequelize.INTEGER,
				allowNull: false,
			},
			createdAt: {
				type: Sequelize.DATE,
			},
			updatedAt: {
				type: Sequelize.DATE,
			},
		});
	},
	down: async (queryInterface) => {
		await queryInterface.dropTable('flight_departure_details');
	},
};
