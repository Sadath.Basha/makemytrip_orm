module.exports = {
	up: async (queryInterface) => {
		const date = new Date();
		await queryInterface.bulkInsert(
			'airline',
			[
				{
					name: 'SpiceJet',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'IndiGo',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'Air India',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'GoAir',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'AirAsia India',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'Vistara',
					createdAt: date,
					updatedAt: date,
				},
				{
					name: 'Alliance Air',
					createdAt: date,
					updatedAt: date,
				},
			],
			{}
		);
	},

	down: async (queryInterface) => {
		await queryInterface.bulkDelete('airline', null, {});
	},
};
