const path = require('path');
const Umzug = require('umzug');
const sequelize = require('./connection');

const umzug = new Umzug({
	migrations: {
		path: path.join(__dirname, '../migrations'),
		params: [sequelize.getQueryInterface()],
	},
	storage: 'sequelize',
	storageOptions: {
		sequelize,
	},
});

module.exports = umzug;
