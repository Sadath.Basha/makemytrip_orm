const mailgun = require('mailgun-js');

const sendMail = (email, subject, message) => {
	const mail = mailgun({ apiKey: process.env.API, domain: process.env.DOMAIN });
	return new Promise((resolve, reject) => {
		const data = {
			from: 'MakeMyTrip <no-reply@makemytrip-clone.com>',
			to: `${email}`,
			subject: `${subject}`,
			html: `${message}`,
		};

		mail.messages().send(data, function (error, body) {
			if (error) {
				return reject(error);
			}

			return resolve(body);
		});
	});
};

module.exports = sendMail;
