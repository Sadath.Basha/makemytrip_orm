const jwt = require('jsonwebtoken');

module.exports = (request, response, next) => {
	const token = request.header('accessToken');

	if (!token) {
		return response.status(401).send('Access denied.');
	}

	try {
		const verified = jwt.verify(token, process.env.TOKEN_SECRET);
		request.user = verified;

		return next();
	} catch (error) {
		return response.status(400).json({ message: 'Invalid token' });
	}
};
