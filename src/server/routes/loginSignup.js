const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require('uuid');

const router = express.Router();

const userSchema = require('../validator/user');
const {
	createNewUser,
	findUserByEmail,
	findUserById,
	updateVerificationStatus,
} = require('../../controllers/users');

const sendMail = require('../mailServer');

router.post('/signup', async (request, response) => {
	try {
		const { name, email, password } = request.body;
		const validationResult = userSchema.signup(request.body);

		if (validationResult.error) {
			return response.status(400).json({
				errors: validationResult.error.details[0],
			});
		}

		const findUserResult = await findUserByEmail(email);

		if (findUserResult === null) {
			const uuid = uuidv4();
			const salt = await bcrypt.genSalt(10);
			const hashedPassword = await bcrypt.hash(password, salt);

			const newUser = {
				id: uuid,
				name,
				email,
				phone: '',
				password: hashedPassword,
				verify_email: 0,
			};

			await createNewUser(newUser);

			const verifyLink = `https://makemytrip-backend.herokuapp.com/api/verify/${uuid}/`;
			const subject = 'Verify Your Email To Continue Booking Flights';
			const message = `<body style="margin: 0; padding: 0">
			<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td style="padding: 20px 0 30px 0">
						<table
							align="center"
							border="0"
							cellpadding="0"
							cellspacing="0"
							width="600"
							style="border-collapse: collapse; border: 1px solid #cccccc"
						>
							<tr>
								<td align="center" bgcolor="#134175" style="padding: 40px 0 30px 0">
									<img
										src="https://imgak.mmtcdn.com/pwa_v3/pwa_hotel_assets/header/mmtLogoWhite.png"
										alt="MakeMyTrip Logo"
									/>
								</td>
							</tr>
							<tr>
								<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px">
									<table
										border="0"
										cellpadding="0"
										cellspacing="0"
										width="100%"
										style="border-collapse: collapse; text-align: center"
									>
										<tr>
											<td style="color: #153643; font-family: Arial, sans-serif">
												<h1>Thanks for signing up, ${name}!</h1>
											</td>
										</tr>
										<tr>
											<td>
												<p>Please verify your email address to book different flights.</p>
												<h2>Thank you!</h2>
												<a
													href="${verifyLink}"
													style="
														background-color: #ffbe00;
														border: 1px solid #ffbe00;
														border-color: #ffbe00;
														border-radius: 0px;
														border-width: 1px;
														color: #000000;
														display: inline-block;
														font-size: 14px;
														font-weight: normal;
														letter-spacing: 0px;
														line-height: normal;
														padding: 12px 40px 12px 40px;
														text-align: center;
														text-decoration: none;
														border-style: solid;
														font-family: inherit;
													"
													target="_blank"
													>Verify Email Now</a
												>
											</td>
										</tr>
										<tr>
										<td style="padding: 1rem">
											You can also verify your account by clicking this link
											<a
												href="http://localhost:3000/api/verify/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6IlNoeWFtIE1haGF0byIsInVzZXJFbWFpbCI6InNoeWFtamFuYW1tYWh0b0BnbWFpbC5jb20iLCJpYXQiOjE2MTA0MzY3NDJ9.URnvnkkaO4Ak5kNcjsmcnNiIdho3p7aaSO4ecgFfTk8"
												>Click here</a
											>
										</td>
										</tr>
										<tr>
											<td>
												<p>Thank you for choosing us as your primary travel partner.</p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</body>`;

			await sendMail(email, subject, message);

			return response.status(200).json({
				message: 'Successfully registered. Check you email to verify your account.',
			});
		}

		return response.status(409).json({
			message: 'Email already exists.',
		});
	} catch (error) {
		return response.status(400).json({ message: error });
	}
});

router.post('/login', async (request, response) => {
	try {
		const { error } = userSchema.login(request.body);

		if (error) {
			return response.status(401).json({ message: error.details[0].message });
		}

		const userEmail = request.body.email;
		const userPassword = request.body.password;
		const findUserResult = await findUserByEmail(userEmail);

		if (findUserResult !== null) {
			if (findUserResult.dataValues.verify_email === 0) {
				return response.status(401).json({ message: 'Please verify you email' });
			}

			const validPassword = await bcrypt.compare(userPassword, findUserResult.dataValues.password);

			if (validPassword) {
				const token = jwt.sign(
					{
						id: findUserResult.dataValues.id,
					},
					process.env.TOKEN_SECRET,
					{ expiresIn: '10m' }
				);

				return response
					.header('accessToken', token)
					.json({ token, message: 'Successfully logged in.', id: findUserResult.dataValues.id });
			}

			return response.status(401).json({
				message: 'Invalid password',
			});
		}

		response.status(403).json({
			message: 'Invalid credentials.',
		});
	} catch (error) {
		return response.status(400).json({
			message: error.message,
		});
	}

	return 0;
});

router.get('/verify/:id', async (request, response) => {
	try {
		const findUserResult = await findUserById(request.params.id);

		if (findUserResult !== null) {
			await updateVerificationStatus(request.params.id, { verify_email: 1 });

			return response.send(`<h3 style="text-align:center">Email verified Successfully!</h3>`);
		}

		return response.status(400).json({
			message: 'Invalid Link',
		});
	} catch (error) {
		return response.status(400).json({
			message: error.message,
		});
	}
});

module.exports = router;
