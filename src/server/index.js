const express = require('express');
const cors = require('cors');
const path = require('path');

const umzug = require('../dbConnection/migration');

const app = express();
app.use(cors());

app.use(express.static(path.join(__dirname, '../public')));

app.use(express.json());

app.use(
	express.urlencoded({
		extended: false,
	})
);

app.use('/api', require('./routes/loginSignup'));

app.use('*', (request, response) => {
	response.json({
		message: '404 Page Not Found.',
	});
});

app.listen(process.env.PORT, () => {
	console.log(`Listening on port ${process.env.PORT}`);

	(async () => {
		try {
			await umzug.up();
			console.log('All migrations performed successfully');
		} catch (error) {
			console.log(error);
		}
	})();
});
