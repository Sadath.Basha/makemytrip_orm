const Joi = require('joi');

const login = (data) => {
	const schema = Joi.object({
		email: Joi.string().email().required(),
		password: Joi.string().required(),
	});

	return schema.validate(data);
};

const signup = (data) => {
	const schema = Joi.object({
		name: Joi.string().min(4).required().label('Name'),
		email: Joi.string().email().required().label('Email'),
		password: Joi.string().min(7).required().label('Password'),
		confirmPassword: Joi.string()
			.required()
			.equal(Joi.ref('password'))
			.label('Confirm password')
			.options({ messages: { 'any.only': '{{#label}} does not match' } }),
	});

	return schema.validate(data);
};

module.exports = { login, signup };
