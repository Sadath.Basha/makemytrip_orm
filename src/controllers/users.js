const Users = require('../models/user');

const createNewUser = (userObj) => {
	return new Promise((resolve, reject) => {
		Users()
			.create(userObj)
			.then((data) => {
				resolve(data);
			})
			.catch((error) => {
				reject(error);
			});
	});
};

const findUserByEmail = (email) => {
	return new Promise((resolve, reject) => {
		Users()
			.findOne({ where: { email } })
			.then((data) => {
				resolve(data);
			})
			.catch((error) => {
				reject(error);
			});
	});
};

const findUserById = (id) => {
	return new Promise((resolve, reject) => {
		Users()
			.findOne({ where: { id } })
			.then((data) => {
				resolve(data);
			})
			.catch((error) => {
				reject(error);
			});
	});
};

const updateVerificationStatus = (id, updateObj) => {
	return new Promise((resolve, reject) => {
		Users()
			.update(updateObj, {
				where: { id },
			})
			.then((data) => {
				resolve(data);
			})
			.catch((error) => {
				reject(error);
			});
	});
};

module.exports = {
	createNewUser,
	findUserByEmail,
	findUserById,
	updateVerificationStatus,
};
