const Sequelize = require('sequelize');
const connection = require('../dbConnection/connection');

const Booking = () => {
	return connection.define('booking', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		flight_price_details_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'flight_price_details',
				key: 'id',
			},
			allowNull: false,
		},
		user_id: {
			type: Sequelize.STRING,
			references: {
				model: 'users',
				key: 'id',
			},
			allowNull: false,
		},
		confirm_status: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		payment_status: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});
};

module.exports = Booking;
