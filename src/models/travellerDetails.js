const Sequelize = require('sequelize');
const connection = require('../dbConnection/connection');

const Booking = () => {
	return connection.define('traveller_details', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		booking_id: {
			type: Sequelize.INTEGER,
			references: {
				model: 'booking',
				key: 'id',
			},
			allowNull: false,
		},
		first_name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		last_name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		gender: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		email: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		phone: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		country_code: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});
};

module.exports = Booking;
