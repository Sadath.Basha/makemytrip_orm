const Sequelize = require('sequelize');
const connection = require('../dbConnection/connection');

const FlightClass = () => {
	return connection.define('flight_class', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
	});
};

module.exports = FlightClass;
