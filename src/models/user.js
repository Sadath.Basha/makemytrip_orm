const Sequelize = require('sequelize');
const connection = require('../dbConnection/connection');

const User = () => {
	return connection.define('users', {
		id: {
			type: Sequelize.STRING,
			primaryKey: true,
		},
		name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		email: {
			type: Sequelize.STRING,
			allowNull: false,
			unique: true,
		},
		phone: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		password: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		verify_email: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});
};

module.exports = User;
